
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, BooleanType, StructType}
import org.apache.spark.sql.functions._


object Processor {

  def main(args: Array[String]): Unit = {

//    import spark.implicits._

    val spark = SparkSession
      .builder
      .appName("croissant-project")
      .master("local[*]")
      .getOrCreate()

    // hide warnings
    spark.sparkContext.setLogLevel("ERROR")

    val msgSchema = new StructType()
      .add("sensorId", StringType, nullable = true)
      .add("longitude", DoubleType, nullable = true)
      .add("latitude", DoubleType, nullable = true)
      .add("formed", DoubleType, nullable = true)
      .add("superSecretKey", StringType, nullable = true)
      .add("active", BooleanType, nullable = true)
      .add("value", IntegerType, nullable = true)

    val df = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "kafka:9092")
      .option("subscribe", "topic_A")
      .load()

    val dfString = df.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")

    val dfProcessed = dfString
      .select(from_json(col("value"), msgSchema).as("data"))
      .select("data.*")
      .withColumn("timestamp", col("formed").cast("timestamp"))

    val dfFiltered = dfProcessed
      .filter(dfProcessed("active") === "true")

    val dfAggregated = dfFiltered
      .withWatermark("timestamp", "10 seconds")
      .groupBy(window(col("timestamp"), "10 seconds", "10 seconds"))
      .agg(avg("value").alias("meanValue"))


//    ////// WRITE TO CONSOLE
//    val query = dfAggregated
//      .writeStream
//      .format("console")
//      .option("truncate", false)
//      .outputMode("append")
//      .start()
//      .awaitTermination()

    //// WRITE TO KAFKA
    val query = dfAggregated
      .select(to_json(struct("*")).alias("value"))
      .writeStream
      .format("kafka")
      .outputMode("append")
      .option("topic", "topic_B")
      .option("kafka.bootstrap.servers", "kafka:9092")
      .option("checkpointLocation", "/tmp/checkpoint")
      .start()
      .awaitTermination()

    spark.stop()
  }

}

