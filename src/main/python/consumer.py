from kafka import KafkaConsumer
import json

class MessageConsumer:
    broker = ""
    topic = ""
    group_id = ""
    logger = None

    def __init__(self, broker, topic, group_id="consumer-group"):
        self.broker = broker
        self.topic = topic
        self.group_id = group_id

    def activate_listener(self):
        consumer = KafkaConsumer(bootstrap_servers=self.broker,
                                 group_id=self.group_id,
                                 consumer_timeout_ms=200000,
                                 auto_offset_reset='earliest',
                                 enable_auto_commit=False,
                                 value_deserializer=lambda m: json.loads(m.decode('ascii')))

        consumer.subscribe(self.topic)
        print("consumer is listening....", flush=True)
        try:
            for message in consumer:
                print("Received message: " + str(message.value), flush=True)
                consumer.commit()
        except KeyboardInterrupt:
            print("Aborted by user...", flush=True)
        finally:
            consumer.close()


consumer1 = MessageConsumer(broker="kafka:9092", topic="topic_B")
consumer1.activate_listener()