from kafka import KafkaProducer
import json, random, time


class MessageProducer:
    broker = ""
    topic = ""
    producer = None

    def __init__(self, broker, topic):
        self.broker = broker
        self.topic = topic
        self.producer = KafkaProducer(bootstrap_servers=self.broker,
                                      value_serializer=lambda v: json.dumps(v).encode('utf-8'),
                                      acks='all',
                                      retries=3)

    def send_msg(self):
        try:
            msg = self.generate_random_data()
            future = self.producer.send(self.topic, msg)
            self.producer.flush()
            future.get(timeout=60)
            print("Sent message at time: " + str(time.time()), flush=True)
        except Exception as ex:
            print(ex, flush=True)

    def generate_random_data(self):
        return {
            "sensorId": "a1b2c3d4e5",
            "longitude": 9.237934,
            "latitude": 45.481189,
            "superSecretKey": "kineton",
            "formed": time.time(),
            "active": True if random.random() > 0.5 else False,
            "value": int(random.random() * 99 + 1),
        }


message_producer = MessageProducer(broker="kafka:9092", topic="topic_A")

while True:
    message_producer.send_msg()
    time.sleep(0.5)
