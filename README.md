
# Croissant Project
Author: Leonardo Riva


### Description
Apache Spark & Kafka project 


### Requirements
- Docker 23.0.1


### Procedure

- Open a terminal in the croissant-project directory

- Create Docker images from the three Dockerfiles: `docker build -t ImageName -f DockerfileName`

- Create the JAR executable file from the Scala Processor code: `sbt assembly`

- Run the containers: `docker-compose up`


